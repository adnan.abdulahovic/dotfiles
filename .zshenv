# Zsh env variables

# Default apps
EDITOR=vim
BROWSER=firefox
PLAYER=mpv
VIEWER=sxiv

# Correction prompt
SPROMPT="Correct $fg[red]%R$reset_color to $fg[green]%r?$reset_color (y,n,a,e) "

# History file
HISTSIZE=2000
HISTFILE=~/.zsh_history
SAVEHIST=2000
HISTCONTROL=erasedups

#Misc
COLORTERM="yes"
TZ="Europe/Sarajevo"
KEYTIMEOUT=1
export PAGER=less
#export XDG_RUNTIME_DIR=~/.config

#export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
#export QT_QPA_PLATFORM=wayland-egl

export WLC_REPEAT_RATE=50
export WLC_REPEAT_DELAY=200

export MOZ_ENABLE_WAYLAND=1
. "$HOME/.cargo/env"

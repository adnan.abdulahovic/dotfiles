# Setup fzf
# ---------
if [[ ! "$PATH" == */home/ado/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/ado/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/ado/.fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/home/ado/.fzf/shell/key-bindings.zsh"
